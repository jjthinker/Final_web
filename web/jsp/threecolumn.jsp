<%-- 
    Document   : threecolumn
    Created on : 05-oct-2016, 20:02:33
    Author     : Juan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>CONTROL DE ACCESO</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <link href='http://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'>
        <!-- Codigo JS-->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="../js/skel.min.js" type="text/javascript"></script>
        <script src="../js/skel-panels.min.js" type="text/javascript"></script>
        <script src="../js/init.js" type="text/javascript"></script>
        <script src="../js/main.js" type="text/javascript"></script>
        <script src="../js/html5shiv.js" type="text/javascript"></script>
        <!--Codigo css3-->
        <link href="../css/style-desktop.css" rel="stylesheet" type="text/css"/>
        <link href="../css/skel-noscript.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <link href="../css/ie/v8.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style-1000px.css" rel="stylesheet" type="text/css"/>
        <link href="../css/login_Style.css" rel="stylesheet" type="text/css"/>
        <link href="../css/Style_re.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="header">
            <div class="container">
                <div id="logo">
                    <h1><a href="#">REGISTRO DE USUARIOS NUEVOS</a></h1>
                </div>
                <nav id="nav">
                    <ul>
                        <li><a href="index.jsp">PAGINA DE INICIO</a></li>
                        <li class="active"><a href="threecolumn.jsp">REGISTRAR USUARIOS</a></li>
                        <li><a href="twocolumn1.jsp">CONSULTAR</a></li>
                        <li><a href="twocolumn2.jsp">EDITAR USUARIOS</a></li>
                        <li><a href="onecolumn.jsp">GENERAR REPORTES</a></li>
                    </ul>
                </nav>

            </div>
        </div>
        <div align='center' id="main">
            <h2>Formulario de registro</h2>
            <form action="registrar" method="post">             
                <input name='codigo' placeholder='codigo' type='text' />
                <input name='nombre' placeholder='Nombre' type="text" />
                <input name='apellido1' placeholder='Primer apellido' type='text'/>             
                <input name='apellido2' placeholder='Segundo apellido' type='text'/>             
                <input name='fecha_cumple' placeholder='Fecha cumpleaños' type='text' />                
                <input name='Esta_civil' placeholder='Estado civil' type='text' />               
                <input name='pais' placeholder='Pais' type='text' />             
                <input name='ciudad' placeholder='Ciudad' type='text' />                
                <input name='telefono' placeholder='Telefono' type='text' />                
                <input name='celular' placeholder='Celular' type='text' />                
                <input name='correo' placeholder='Correo' type='text' />                
                <input name='direccion' placeholder='Direccion' type='text'/>               
                <input name='documento' placeholder='Documento' type='text' />          
                <input name='num_docu' placeholder='Numero documento' type='text' />               
                <input name='genero' placeholder='Genero' type='text' />              
                <input name='niv_edu' placeholder='Nivel educativo' type='text' />              
                <input name='titulo' placeholder='Titulo' type='text' />             
                <input name='area' placeholder='Area' type='text' />              
                <input name='usuario' placeholder='Usuario' type='text' />           
                <input name='pass' placeholder='Contraseña' type='text' />            
                <hr><input type='button' value='Registrar'/>
            </form>
        </div>
        <div id="Uniminuto">
            <div class="container">
                <a>UNIMINUTO - PROGRAMACION WEB</a> 
            </div>
        </div>

    </body>
</html>

