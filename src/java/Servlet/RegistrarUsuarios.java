/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controladora.Consultas;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Juan
 */
public class RegistrarUsuarios extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
       
        String codigo = request.getParameter("codigo");
        String nombre = request.getParameter("nombre");
        String apellido1 = request.getParameter("Apellido1");
        String apellido2 = request.getParameter("Apellido2");
        String fecha_cumple  = request.getParameter("Fecha_de_cumpleaños"); 
        String estado_civil = request.getParameter("Estado_Civil");
        String pais = request.getParameter("pais");
        String ciudad  = request.getParameter("ciudad");
        String telefono = request.getParameter("telefono");
        String celular = request.getParameter("celular");
        String correo = request.getParameter("correo");
        String direccion = request.getParameter("direccion");
        String documento = request.getParameter("documento");
        String numero_documento  = request.getParameter("numero_documento");
        String  genero= request.getParameter("genero");
        String nivel_educativo = request.getParameter("nivel_educativo");
        String titulo = request.getParameter("titulo");
        String area = request.getParameter("area");
        String usuario = request.getParameter("usuario");
        String contraseña = request.getParameter("pass");
        
        Consultas co = new Consultas();
        if (co.registrar(nombre, apellido1, usuario, contraseña)) {
            response.sendRedirect(usuario);
        } else {
            response.sendRedirect("threecolumn.jsp");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
