<%-- 
    Document   : twocolumn1
    Created on : 05-oct-2016, 20:03:34
    Author     : Juan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>CONTROL DE ACCESO</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <link href='http://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'>
        <!-- Codigo JS-->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="../js/skel.min.js" type="text/javascript"></script>
        <script src="../js/skel-panels.min.js" type="text/javascript"></script>
        <script src="../js/init.js" type="text/javascript"></script>
        <script src="../js/main.js" type="text/javascript"></script>
        <script src="../js/html5shiv.js" type="text/javascript"></script>
        <!--Codigo css3-->
        <link href="../css/style-desktop.css" rel="stylesheet" type="text/css"/>
        <link href="../css/skel-noscript.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <link href="../css/ie/v8.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style-1000px.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="header">
            <div class="container">
                <div id="logo">
                    <h1><a href="#">CONSULTAR REGISTRO DE USUARIOS</a></h1>
                </div>
                <nav id="nav">
                    <ul>
                        <li><a href="index.jsp">PAGINA DE INICIO</a></li>
                        <li><a href="threecolumn.jsp">REGISTRAR USUARIO</a></li>
                        <li class="active"><a href="twocolumn1.jsp">CONSULTAR</a></li>
                        <li><a href="twocolumn2.jsp">EDITAR USUARIOS</a></li>
                        <li><a href="onecolumn.jsp">GENERAR REPORTES</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <div id="main">
           <div align="center"> 
           <table border="0" width="600" style="font-family: Verdana; font-size: 8pt" id="table1"> 
               <tr> 
                   <td colspan="2"><h3 align="center">Actualización de datos</h3></td> 
               </tr> 
               <form method="POST" action="form2.php"> 
                   <tr> 
                       <td width="50%">&nbsp;</td> 
                       <td width="50%">&nbsp;</td> 
                   </tr> 
                   <tr> 
                       <td width="50%"> 
                           <p align="center"><b>ID del registro a actualizar: </b></td> 
                       <td width="50%"> 
                           <p align="center"><input type="text" name="id" size="20"></td> 
                   </tr> 
                   <tr> 
                       <td width="50%">&nbsp;</td> 
                       <td width="50%">&nbsp;</td> 
                   </tr> 
                   <tr> 
                       <td width="100%" colspan="2"> 
                           <p align="center"> 
                           <input type="submit" value="Iniciar actualización" name="B1"></td> 
                   </tr> 
               </form> 
           </table> 
        </div> 
        </div>
        <div id="Uniminuto">
            <div class="container">
                <a>UNIMINUTO - PROGRAMACION WEB</a> 
            </div>
        </div>

    </body>
</html>
