/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladora;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Juan
 */
public class Consultas  extends Conexión{
    
    public boolean autenticacion(String usuario, String Contraseña){
        PreparedStatement pst = null;
        ResultSet rs=null;
        
        try{
            String consulta="select * from loginjsp where usuario= ?  and pass= ?";
            pst = getConexión().prepareStatement(consulta);
            pst.setString(1, usuario);
            pst.setString(2, Contraseña);
            rs=pst.executeQuery();
            
            if(rs.absolute(1)){
                return true;
            }
           
        }catch(Exception e){
            System.err.println("Error"+e);
        }finally{
            try{
                if(getConexión()!=null) getConexión().close();
                if(pst !=null) pst.close();
                if (rs !=null)rs.close();
            }catch(Exception e){
                 System.err.println("Error"+e);
            }
        }
        
        return false;
    }
    
    public boolean registrar (String nombre,String apellido,String usuario,String contraseña){
        
        PreparedStatement pst=null;
        
        try{
            String consulta = "insert into loginjsp (nombre,Apellido,usuario,pass) values (?,?,?,?)";
            pst =getConexión().prepareStatement(consulta);
            pst.setString(1, nombre);
            pst.setString(2, apellido);
            pst.setString(3, nombre);
            pst.setString(4, contraseña);
            
            if(pst.executeUpdate() ==1){
                return true;
            }
        }catch(Exception ex){
            System.err.println("Error"+ex);
        }finally{
            try{
                if(getConexión() != null) getConexión().close();
                if(pst !=null) pst.close();
            }catch (Exception ex){
                System.err.println("Error"+ex);
            }
        }
        
        return false;
    }
}
